<?php

const HTTP_CODE_STRINGS = [
    200 => 'OK',
    404 => 'Not found'
];

/**
 * @param string $uri
 * @return array<string, string|null>
 * @throws Exception
 */
function request_parse_uri(string $uri): array
{
    if(!preg_match('/(?<path_info>[^?]+)(\?(?<query_string>.+))?/', $uri, $match)){
        throw new \Exception(sprintf('Could not parse URI %s', $uri));
    }

    $queryString = $match['query_string'];

    $query = [];
    if($queryString) parse_str($queryString, $query);

    return [
        'path_info' => $match['path_info'],
        'query_string' => $match['query_string'] ?? null,
        'query' => $query,
    ];
}

/**
 * @param string $method
 * @param string $uri
 * @return array<string, mixed>
 * @throws Exception
 */
function request_create(string $method, string $uri): array
{
    $parsedUri = request_parse_uri($uri);

    return [
        'host' => 'localhost',
        'method' => $method,
        'uri' => $uri,
        'post' => [],
        'files' => [],
        ...$parsedUri
    ];
}

/**
 * @return array<string, mixed>
 */
function request_create_from_globals(): array
{
    $request = request_create($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
    $request['host'] = $_SERVER['HTTP_HOST'];
    $request['post'] = $_POST;
    $request['files'] = $_FILES;
    return $request;
}

function response_create(?string $content, int $httpCode = 200, array $headers = [])
{
    return [
        'content' => $content,
        'http_code' => $httpCode,
        'headers' => $headers,
    ];
}

function response_send(array $response): void
{
    header(sprintf('HTTP/1.1 %s %s', $response['http_code'], HTTP_CODE_STRINGS[$response['http_code']]));
    foreach($response['headers'] as $key => $value){
        header(sprintf('%s: %s', $key, $value));
    }
    if($response['content'] !== null) echo $response['content'];
}