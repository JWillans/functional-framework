<?php

function fs_find_php(string $dir): array
{
    return fs_find($dir, '/\.php$/');
}

function fs_find(string $dir, string $regex): array
{
    $names = scandir($dir);
    $matches = [];
    foreach($names as $name){
        if($name === '..' || $name === '.') continue;
        $path = $dir . '/' . $name;
        if(is_dir($path)){
            $matches = array_merge($matches, fs_find($path, $regex));
        }
        elseif(preg_match($regex, $path)){
            $matches[] = $path;
        }
    }
    return $matches;
}