<?php

function t_map(array $items, callable $callback): string
{
    $items = array_map($callback, $items);
    return implode('', $items);
}

function t_escape(string $text): string
{
    return mb_convert_encoding($text, 'HTML-ENTITIES');
}

function template_render(string $name, array $context = []): string
{
    $path = __DIR__ . '/../app/templates/' . $name . '.php';

    // Expose functions for heredoc
    $_map = 't_map'; // React style mapping
    $_path = 'route_generate';
    $_escape = 't_escape';

    if(!file_exists($path)) throw new Exception(sprintf('Template %s does not exist', $path));
    $blocks = [];
    ob_start();
    include $path;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}