<?php

function debug_dump($data): void
{
    $backtrace = debug_backtrace();
    $prevCall = $backtrace[0];

    echo '<div style="background: #56565d; color: #d2c7c7; padding: 0.5em; font-family: sans-serif; font-size: 14px;">' .
        '<div>' . $prevCall['file'] . ':' . $prevCall['line'] . '</div>' .
        '<pre style="background: #2b2b2d; padding: 0.5rem; margin: 0.5em 0 0 0;">' . print_r($data, true) . '</pre>' .
        '</div>'
    ;
}