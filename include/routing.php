<?php

$routes = [];

/**
 * @param string $name
 * @param string $pattern
 * @param string $controller
 * @param array<int, string> $methods
 * @return void
 */
function route_define(string $name, string $pattern, string $controller, array $methods = ['GET']): void
{
    global $routes;
    $routes[$name] = [
        'name' => $name,
        'pattern' => $pattern,
        'controller' => $controller,
        'methods' => $methods,
    ];
}

function route_generate(string $name, array $parameters = []): string
{
    global $routes;
    if(!array_key_exists($name, $routes)) throw new \Exception(sprintf('Route %s does not exist', $name));
    $route = $routes[$name];
    $pattern = $route['pattern'];

    return preg_replace_callback('/\{(?<name>[^\}]+)\}/', function(array $match) use ($parameters, $name){
        $parameterName = $match['name'];
        if(!array_key_exists($parameterName, $parameters)) throw new Exception(sprintf('Missing required parameter "%s" for "%s"', $parameterName, $name));
        return (string)$parameters[$parameterName];
    }, $pattern);
}

function route_pattern_regex(string $pattern): string
{
    $regex = preg_replace_callback('#\{(?<name>.+)\}#', function(array $match){
        return sprintf('(?<%s>[^/]+)', $match['name']);
    }, $pattern);
    return sprintf('#^%s$#', $regex);
}

/**
 * @param string $pattern
 * @param string $pathInfo
 * @return array<string, mixed>|null
 */
function route_pattern_match(string $pattern, string $pathInfo): ?array
{
    $regex = route_pattern_regex($pattern);

    if(!preg_match($regex, $pathInfo, $match)) return null;

    $keys = array_keys($match);
    foreach($keys as $key){
        if(is_numeric($key)) unset($match[$key]);
    }

    return $match;
}

/**
 * @param array<string, mixed> $request
 * @return array<string, mixed>
 * @throws Exception
 */
function route_match(array $request): array
{
    global $routes;
    foreach($routes as $route){
        if(!in_array($request['method'], $route['methods'])) continue;

        $parameters = route_pattern_match($route['pattern'], $request['path_info']);
        if($parameters === null) continue;

        return [
            ...$route,
            'parameters' => [...$parameters, ...$request['query']],
            'request' => $request,
        ];
    }
    throw new \Exception(sprintf('No route found for %s %s', $request['method'], $request['path_info']));
}

/**
 * @param array<string, mixed> $routeMatch
 * @return array<string, mixed> HTTP response
 * @throws ReflectionException
 */
function route_match_invoke(array $routeMatch): array
{
    $ctrlRef = new ReflectionFunction($routeMatch['controller']);
    $paramRefs = $ctrlRef->getParameters();

    $args = [];
    array_walk($paramRefs, function(ReflectionParameter $paramRef) use (&$args, $routeMatch){
        $name = $paramRef->getName();
        if(!array_key_exists($name, $routeMatch['parameters'])){
            throw new \Exception(sprintf('Controller parameter %s missing from request', $name));
        }
        $args[] = $routeMatch['parameters'][$name];
    });

    return $ctrlRef->invokeArgs($args);
}