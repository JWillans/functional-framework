<?php

route_define('dynamic', '/dynamic/{slug}', 'dynamic_controller', ['GET']);

/**
 * @throws Exception
 */
function dynamic_controller(string $slug): array
{
    return response_create(template_render('dynamic', ['slug' => $slug]));
}