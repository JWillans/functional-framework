<?php

route_define('homepage', '/', 'homepage_controller', ['GET']);

/**
 * @throws Exception
 */
function homepage_controller(): array
{
    return response_create(template_render('homepage'));
}