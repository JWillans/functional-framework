<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $blocks['title'] ?? 'Site Name'; ?></title>
        <?php echo $blocks['bootstrap_stylesheets'] ?? <<<TPL
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        TPL; ?>
    </head>
    <body>
        <?php echo $blocks['header'] ?? template_render('partial/header'); ?>
        <div class="container my-4">
            <?php echo $blocks['content'] ?? ''; ?>
        </div>
        <?php echo $blocks['footer'] ?? template_render('partial/footer'); ?>
        <?php echo $blocks['bootstrap_javascripts'] ?? <<<TPL
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
        TPL; ?>
    </body>
</html>