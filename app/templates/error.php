<?php

/** @var Exception $exception */
$exception = $context['exception'];

$blocks['content'] = <<<TPL
    <h2>Error</h2>
    <p>{$_escape($exception->getMessage())}</p>
    <ul>
        {$_map($exception->getTrace(), function(array $call){
            return sprintf('<li>%s:%s</li>', t_escape($call['file']), t_escape($call['line']));
        })}    
    </ul>
TPL;

include __DIR__ . '/base.php';