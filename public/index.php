<?php

try{
    require __DIR__ . '/../bootstrap.php';

    $request = request_create_from_globals();
    $routeMatch = route_match($request);
    $response = route_match_invoke($routeMatch);
    response_send($response);
}
catch(Exception|TypeError $e){
    $errorResponse = response_create(template_render('error', ['exception' => $e]), 500);
    response_send($errorResponse);
}
