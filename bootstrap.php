<?php

require __DIR__ . '/include/debug.php';
require __DIR__ . '/include/fs.php';
require __DIR__ . '/include/http.php';
require __DIR__ . '/include/routing.php';
require __DIR__ . '/include/template.php';

$controllerPaths = fs_find_php(__DIR__ . '/app/controller');
foreach($controllerPaths as $controllerPath){
    require $controllerPath;
}